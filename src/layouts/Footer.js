import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { useTranslation } from "react-i18next";
import "../index.css";
import { Box, Typography } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  footer: {
    backgroundColor: "#2f2d2e",
    borderTop: "solid 2px #f15025",
    fontSize: "1.2rem",
    textAlign: "center",
    padding: theme.spacing(1, 0),
    [theme.breakpoints.down("sm")]: {
      fontSize: ".9rem",
    },
  },
  con: {
    [theme.breakpoints.down("sm")]: {
      fontSize: ".9rem",
    },
  },
}));

export default function Footer() {
  const classes = useStyles();
  const [t] = useTranslation("global");
  return (
    <>
      <Box
        component={"footer"}
        className={classes.footer}
        sx={{ maxWidth: "114rem", margin: "0 auto" }}
        mt={20}
      >
        <Box component={"div"} sx={{ display: "inline-block;" }}>
          <Box my={1} sx={{ display: "block" }} component={"small"}>
            {t("Presentación.Derecho1")}{" "}
            <Typography
              className={classes.con}
              component={"span"}
              color="primary"
            >
              CodigoArchivo
            </Typography>{" "}
            {t("Presentación.Derecho2")} {new Date().getFullYear()}
          </Box>
          <Box my={1} sx={{ display: "block" }} component={"small"}>
            {t("Presentación.Creado")}{" "}
            <Typography
              className={classes.con}
              component={"span"}
              color="primary"
            >
              Jackson Quintero
            </Typography>
          </Box>
        </Box>
      </Box>
    </>
  );
}
