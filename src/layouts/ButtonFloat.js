import * as React from "react";
import { Box, Fab, makeStyles } from "@material-ui/core";
import { Facebook, Instagram, Telegram } from "@material-ui/icons";
const useStyles = makeStyles((theme) => ({
  root: {
    position: "fixed",
    bottom: theme.spacing(2),
    right: theme.spacing(2),
    marginRight: theme.spacing(10),
    zIndex: theme.spacing(10),
    [theme.breakpoints.down("xs")]: {
      right: theme.spacing(0),
    },
  },
  fa: {
    margin: theme.spacing(0, 1),
  },
}));
export default function ButtonFloat() {
  const classes = useStyles();
  return (
    <Box
      component={"div"}
      sx={{ "& > :not(style)": { m: 1 } }}
      className={classes.root}
    >
      <Fab
        href={"https://t.me/jackyire"}
        target={"_blank"}
        className={classes.fa}
        size="small"
        color="inherit"
        aria-label="add"
        >
        <Telegram color="secondary" />
      </Fab>
      <Fab
        href={"https://www.instagram.com/codigoarchivo/?hl=es"}
        target={"_blank"}
        className={classes.fa}
        size="small"
        color="inherit"
        aria-label="edit"
        >
        <Instagram color="secondary" />
      </Fab>
      <Fab
        href={"https://www.facebook.com/groups/codigoparaprogramadores"}
        target={"_blank"}
        className={classes.fa}
        size="small"
        color="inherit"
        aria-label="edit"
      >
        <Facebook color="secondary" />
      </Fab>
    </Box>
  );
}
