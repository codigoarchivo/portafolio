import React from "react";
import AppBar from "@material-ui/core/AppBar";
import MenuIcon from "@material-ui/icons/Menu";
import Toolbar from "@material-ui/core/Toolbar";
import PersonIcon from "@material-ui/icons/Person";
import SchoolIcon from "@material-ui/icons/School";
import PagesIcon from "@material-ui/icons/Pages";
import StarsIcon from "@material-ui/icons/Stars";
import IconButton from "@material-ui/core/IconButton";
import FolderSpecialIcon from "@material-ui/icons/FolderSpecial";
import ImportantDevicesIcon from "@material-ui/icons/ImportantDevices";

import { makeStyles } from "@material-ui/core/styles";

import { useTranslation } from "react-i18next";

import "../index";

const useStyles = makeStyles((theme) => ({
  appbar: {
    color: "#fff",
    borderBottom: "solid 2px #f15025",
    [theme.breakpoints.down("sm")]: {
      borderBottom: "none",
      backgroundColor: "transparent",
      minHeight: "0px !important",
      boxShadow: "none",
    },
  },
  toobar_A: {
    [theme.breakpoints.down("sm")]: {
      backgroundColor: "#2f2d2e",
      minHeight: "0px !important",
      display: "flex !important",
      marginTop: "1rem",
      flexDirection: "column",
      marginLeft: "1rem",
      width: "10%",
      boxShadow:
        " 0px 2px 4px -1px rgb(0 0 0 / 20%), 0px 4px 5px 0px rgb(0 0 0 / 14%), 0px 1px 10px 0px rgb(0 0 0 / 12%)",
    },
  },
  toobar_B: {
    [theme.breakpoints.down("sm")]: {
      backgroundColor: "#2f2d2e",
      paddingBottom: "1rem",
      display: "none !important",
      flexDirection: "column",
      marginLeft: "1rem",
      width: "10%",
      boxShadow:
        " 0px 2px 4px -1px rgb(0 0 0 / 20%), 0px 4px 5px 0px rgb(0 0 0 / 14%), 0px 1px 10px 0px rgb(0 0 0 / 12%)",
    },
  },
  menuButton: {
    padding: theme.spacing(1),
    margin: theme.spacing(0, 1),
  },
  menuButtonN: {
    [theme.breakpoints.down("sm")]: {
      position: "relative !important",
      right: "-.1rem !important",
    },
  },
  toogleCont: {
    display: "flex !important",
  },
}));

export function Nav() {
  const classes = useStyles();
  const Presentation = (valor) => {
    document.getElementById(valor).scrollIntoView({ behavior: "smooth" });
  };

  const [t, i18n] = useTranslation("global");
  return (
    <>
      {/* text change global */}
      {<>{t()} </>}

      {/* Nav */}
      <AppBar component={"nav"} color="secondary" className={classes.appbar}>
        <Toolbar className={classes.toobar_A} style={{ display: "none" }}>
          <IconButton
            onClick={() =>
              document.getElementById("nav").classList.toggle(classes.toogleCont)
            }
            className={classes.menuButton}
            color="inherit"
            aria-label="menu"
          >
            <MenuIcon />
          </IconButton>
        </Toolbar>
        <Toolbar
          id="nav"
          className={classes.toobar_B}
          style={{ display: "flex" }}
        >
          <IconButton
            onClick={() => Presentation("back-to-top-anchor")}
            className={classes.menuButton}
            edge="start"
            color="inherit"
            aria-label="menu"
          >
            <PersonIcon />
          </IconButton>{" "}
          <IconButton
            onClick={() => Presentation("information")}
            className={classes.menuButton}
            edge="start"
            color="inherit"
            aria-label="menu"
          >
            <StarsIcon />
          </IconButton>{" "}
          <IconButton
            onClick={() => Presentation("Professional")}
            className={classes.menuButton}
            edge="start"
            color="inherit"
            aria-label="menu"
          >
            <ImportantDevicesIcon />
          </IconButton>
          <IconButton
            onClick={() => Presentation("Portfolios")}
            className={classes.menuButton}
            edge="start"
            color="inherit"
            aria-label="menu"
          >
            <PagesIcon />
          </IconButton>
          <IconButton
            onClick={() => Presentation("Experience")}
            className={classes.menuButton}
            edge="start"
            color="inherit"
            aria-label="menu"
          >
            <FolderSpecialIcon />
          </IconButton>
          <IconButton
            onClick={() => Presentation("misEstudios")}
            className={classes.menuButton}
            edge="start"
            color="inherit"
            aria-label="menu"
          >
            <SchoolIcon />
          </IconButton>
          <IconButton
            style={{ position: "absolute", right: "2.5rem" }}
            size="small"
            className={(classes.menuButton, classes.menuButtonN)}
            edge="start"
            color="inherit"
            aria-label="menu"
            onClick={() => i18n.changeLanguage("es")}
          >
            es
          </IconButton>
          <IconButton
            style={{ position: "absolute", right: "4rem" }}
            size="small"
            className={(classes.menuButton, classes.menuButtonN)}
            edge="start"
            color="inherit"
            aria-label="menu"
            onClick={() => i18n.changeLanguage("en")}
          >
            en
          </IconButton>
        </Toolbar>
      </AppBar>
    </>
  );
}
