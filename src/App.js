import React from "react";

import theme from "./theme";
import { Box } from "@material-ui/core";
import CssBaseline from "@material-ui/core/CssBaseline";
import { ThemeProvider } from "@material-ui/core/styles";
import ButtonFloat from "./layouts/ButtonFloat";

import AOS from "aos";
import { FillingBottle } from "react-cssfx-loading/lib";

import { Nav } from "./layouts/Nav";
import Top from "./layouts/Top";
import Presentation from "./components/Presentation";
import About from "./components/About";
import Portfolio from "./components/Portfolio";
import Experience from "./components/Experience";
import Education from "./components/Education";
import { Skills } from "./components/Skills";
import Footer from "./layouts/Footer";

import { GlobalCss } from "./GlobalCss";

import "aos/dist/aos.css";
import "@fontsource/roboto";

AOS.init();
function App() {
  const [change, setChange] = React.useState({
    contenido: "block",
    spiner: true,
  });

  React.useEffect(() => {
    setTimeout(() => {
      setChange({
        contenido: "none",
        spiner: false,
      });
    }, 2600);
  }, []);

  const { contenido, spiner } = change;

  return (
    <>
      <CssBaseline />
      <GlobalCss />
      <ThemeProvider theme={theme}>
        {spiner ? (
          <Box
            component={"div"}
            sx={{
              display: contenido,
              position: "relative",
              height: "100vh",
              zIndex: "2000",
              backgroundColor: "gainsboro",
            }}
          >
            <Box
              component={"div"}
              sx={{
                position: "absolute",
                top: "50%",
                left: "50%",
                transform: "translate(-50%, -50%)",
              }}
            >
              <FillingBottle
                color="#f15025"
                width="100px"
                height="100px"
                duration="3s"
              />
            </Box>
          </Box>
        ) : (
          <>
            <Box
              component={"main"}
              sx={{
                backgroundColor: "gainsboro",
              }}
            >
              <Nav />
              <Presentation />
              <About />
              <Skills />
              <Portfolio />
              <Experience />
              <Education />
              <Top />
              <ButtonFloat />
              <Footer />
            </Box>
          </>
        )}
      </ThemeProvider>
    </>
  );
}

export default App;
