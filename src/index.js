import React from "react";
import ReactDOM from "react-dom";
import i18Next from "i18next";
import { I18nextProvider } from "react-i18next";

import global_es from "./translations/es/global.json";
import global_en from "./translations/en/global.json";

import App from "./App";

i18Next.init({
  interpolation: {
    escapeValue: false,
  },
  lng: "es",
  resources: {
    es: {
      global: global_es,
    },
    en: {
      global: global_en,
    },
  },
});

ReactDOM.render(
  <React.Fragment>
    <I18nextProvider i18n={i18Next}>
      <App />
    </I18nextProvider>
  </React.Fragment>,
  document.getElementById("root")
);
