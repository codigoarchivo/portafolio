import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";

import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import Avatar from "@material-ui/core/Avatar";
import IconButton from "@material-ui/core/IconButton";

import ShareIcon from "@material-ui/icons/Share";
import { useTranslation } from "react-i18next";
import "../index.css";
import { DivideEducation } from "./Divide";

import uvm from "../img/logo-uvm.svg";
import fondoUvm from "../img/fondoUvm.jpg";
import titulo from "../img/titulo.jpg";

import { Backdrop, Box, Button, Container, Modal } from "@material-ui/core";
import { School } from "@material-ui/icons";

const useStyles = makeStyles((theme) => ({
  root: {
    boxShadow: theme.shadows[5],
    borderRadius: theme.spacing(1),
    backgroundImage: `linear-gradient(
      to right bottom,
      rgba(0, 0, 0, 0.7),
      rgba(0, 0, 0, 0.7)
    ),url(${fondoUvm})`,
    backgroundSize: "cover",
    backgroundPosition: "bottom",
    display: "flex",
    justifyContent: "center",
    padding: theme.spacing(5, 0),
    [theme.breakpoints.down("xs")]: {
      padding: theme.spacing(5, 0),
    },
  },

  subtitulo1: {
    textAlign: "center",
    padding: theme.spacing(4, 0),
    display: "table",
    margin: "0 auto",
    [theme.breakpoints.down("xs")]: {
      padding: theme.spacing(2, 0),
    },
  },

  card: {
    backgroundColor: "gainsboro",
    boxShadow: theme.shadows[5],
    border: "solid 2px #f15025",
  },
  avatar: {
    backgroundColor: "#2f2d2e",
    borderBottom: "solid 2px #f15025",
    boxShadow: theme.shadows[5],
    width: "100%",
    "& span": {
      color: "#b7d1da",
      display: "table-row",
    },
  },
  parrafo: {
    fontSize: "1.2rem",
    textIndent: "2rem",
  },
  modal: {
    backgroundColor: "#2f2d2e",
    borderBottom: "solid 2px #f15025",
    boxShadow: theme.shadows[5],
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: 500,
    [theme.breakpoints.down("sm")]: {
      width: 400,
    },
    [theme.breakpoints.down("xs")]: {
      width: 280,
    },
  },
}));

export default function RecipeReviewCard() {
  const classes = useStyles();
  const [t] = useTranslation("global");

  const [open, setOpen] = React.useState(false);
  const handleOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };
  return (
    <>
      <Container>
        <DivideEducation />
        <Grid container className={classes.root}>
          <Grid item xs={11} sm={10} md={8} lg={6} xl={6}>
            <Card className={classes.card}>
              <CardHeader
                data-aos="flip-up"
                data-aos-duration="2000"
                className={classes.avatar}
                color="secondary"
                avatar={
                  <Avatar aria-label="recipe">
                    <Box
                      width={40}
                      component={"img"}
                      src={uvm}
                      alt="Foto"
                    ></Box>
                  </Avatar>
                }
                title={t("Presentación.UNIVERSIDAD")}
                subheader="2016 - 2020"
              />

              <CardContent>
                <Typography
                  variant="h5"
                  color="secondary"
                  className={classes.subtitulo1}
                >
                  {t("Presentación.Ingeniero")}
                </Typography>
                <Typography className={classes.parrafo} color="secondary">
                  {t("Presentación.Estudios")}
                </Typography>
              </CardContent>
              <CardActions disableSpacing>
                <IconButton
                  aria-label="share"
                  href={"http://www.uvm.edu.ve/"}
                  target="_blank"
                  color="primary"
                >
                  <ShareIcon />
                </IconButton>
                <IconButton
                  aria-label="share"
                  color="primary"
                  onClick={handleOpen}
                >
                  <School />
                </IconButton>
              </CardActions>
            </Card>
          </Grid>
        </Grid>
      </Container>

      <>
        <Modal
          open={open}
          onClose={handleClose}
          keepMounted
          aria-labelledby="transition-modal-title"
          aria-describedby="transition-modal-description"
          BackdropComponent={Backdrop}
          closeAfterTransition
          BackdropProps={{
            timeout: 500,
          }}
        >
          <Box component={"div"} className={classes.modal} px={4} py={4}>
            <Box
              component={"img"}
              sx={{ width: "100%", height: "auto" }}
              src={titulo}
              alt="Foto"
              mb={3}
            ></Box>
            <Button
              color="primary"
              variant="outlined"
              size="small"
              onClick={handleClose}
            >
              Cerrar
            </Button>
          </Box>
        </Modal>
      </>
    </>
  );
}
