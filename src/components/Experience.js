import React from "react";
import {
  makeStyles,
  Typography,
  Box,
  CardContent,
  Card,
  Grid,
  Container,
} from "@material-ui/core";

import { useTranslation } from "react-i18next";

import { DivideExperience } from "./Divide";

import LinkedIn from "../img/LinkedIn_Logo.svg";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    justifyContent: "center",
    flexDirection: "column",
    alignItems: "center",
  },
  content: {
    padding: theme.spacing(3, 0),
  },
  parrafo: {
    fontSize: "1.2rem",
    padding: theme.spacing(3, 0),
    textIndent: "2rem",
    [theme.breakpoints.down("xs")]: {
      padding: theme.spacing(2, 0),
    },
  },
  details: {
    display: "flex",
    alignItems: "center",
    flexDirection: "row",
    [theme.breakpoints.down("sm")]: {
      flexDirection: "column",
    },
  },
  card: {
    boxShadow: "none",
    backgroundColor: "gainsboro",
    border: "solid 2px #f15025",
  },
  textPrin: {
    display: "inline",
    [theme.breakpoints.down("sm")]: {
      fontSize: "2.5rem",
    },
    [theme.breakpoints.down("xs")]: {
      fontSize: "2rem",
    },
  },
  content1: {
    width: "70%",
    [theme.breakpoints.down("sm")]: {
      width: "80%",
      marginLeft: theme.spacing(0),
    },
    [theme.breakpoints.down("xs")]: {
      width: "95%",
    },
  },
  content2: {
    width: "30%",
    backgroundColor: "#2f2d2e",
    boxShadow: theme.shadows[5],
    border: "solid 2px #f15025",
    height: "140px",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    marginLeft: theme.spacing(2),

    [theme.breakpoints.down("sm")]: {
      width: "50%",
      marginLeft: theme.spacing(0),
    },
    [theme.breakpoints.down("xs")]: {
      width: "60%",
      height: "120px",
    },
  },
}));

export default function MediaControlCard() {
  const classes = useStyles();
  const [t] = useTranslation("global");
  return (
    <>
      <Container>
        <DivideExperience />
        <Grid container className={classes.root}>
          <Grid
            item
            xs={11}
            sm={10}
            md={8}
            lg={6}
            xl={6}
            className={classes.content}
          >
            <Card className={classes.card}>
              <CardContent className={classes.details}>
                <Box component={"div"} className={classes.content1}>
                  <Typography
                    data-aos="zoom-out-right"
                    data-aos-duration="2000"
                    variant="h3"
                    color="secondary"
                    className={classes.textPrin}
                  >
                    Linkedin
                  </Typography>
                  <Typography
                    data-aos="zoom-out-right"
                    data-aos-duration="2000"
                    variant="h5"
                    color="textSecondary"
                    style={{ display: "table-cell" }}
                  >
                    Jackson Quintero
                  </Typography>
                  <Typography className={classes.parrafo} color="secondary">
                    {t("Presentación.Linkedin")}
                  </Typography>
                </Box>
                <Box
                  component={"div"}
                  className={classes.content2}
                  data-aos="flip-right"
                  data-aos-duration="2000"
                  color="secondary"
                >
                  <Box
                    component={"img"}
                    sx={{ height: "28px" }}
                    src={LinkedIn}
                    alt="Foto"
                    mb={2}
                  ></Box>
                  <Typography variant="subtitle1" color="primary">
                    2019 - {new Date().getFullYear()}
                  </Typography>
                </Box>
              </CardContent>
            </Card>
          </Grid>
        </Grid>
      </Container>
    </>
  );
}
