import React from "react";

import Slider from "react-animated-slider";
import "react-animated-slider/build/vertical.css";

import {
  Button,
  Box,
  Typography,
  makeStyles,
  Container,
} from "@material-ui/core";

import { useTranslation } from "react-i18next";
import { DividePortfolio } from "./Divide";

import CardBase from "./CardBase";

const useStyles = makeStyles((theme) => ({
  content: {
    boxShadow: theme.shadows[5],
    borderRadius: theme.spacing(1),
    padding: theme.spacing(7, 5),
    [theme.breakpoints.down("sm")]: {
      padding: theme.spacing(2),
    },
    [theme.breakpoints.down("xs")]: {
      padding: theme.spacing(6, 2),
    },
  },
  title: {
    [theme.breakpoints.down("sm")]: {
      fontSize: "3rem",
    },
    [theme.breakpoints.down("xs")]: {
      fontSize: "2rem",
    },
  },
  spaceButtom: {
    [theme.breakpoints.down("xs")]: {
      paddingTop: "16px",
      paddingBottom: "16px",
    },
  },
  imgC: {
    [theme.breakpoints.down("xs")]: {
      width: "75px",
    },
  },
}));

export default function MediaCard() {
  const [t] = useTranslation("global");
  const classes = useStyles();

  const { content } = CardBase();
  return (
    <>
      <Container>
        <DividePortfolio />
        {/* helper > CardPorfolio */}
        <Slider direction="vertical">
          {content.map((item, index) => (
            <Box
              className={classes.content}
              component={"div"}
              key={index}
              sx={{
                background: `linear-gradient(
                  to right bottom,
                  rgba(0, 0, 0, 0.7),
                  rgba(0, 0, 0, 0.7)
                ),url('${item.image}') no-repeat  bottom`,
                backgroundSize: "cover",
                backgroundPosition: "center",
              }}
            >
              <Box
                component={"div"}
                sx={{
                  position: "absolute",
                  width: "55%",
                }}
              >
                <Typography
                  style={{ display: "initial" }}
                  className={classes.title}
                  variant="h2"
                >
                  {item.title}
                </Typography>
                <Typography
                  style={{ display: "table-row-group" }}
                  variant="body1"
                >
                  {item.description}
                </Typography>
                <Box
                  py={4}
                  className={classes.spaceButtom}
                  component={"div"}
                  sx={{ display: "inline-block" }}
                >
                  <Button
                    color="primary"
                    variant="outlined"
                    size="medium"
                    href={item.hrefPor}
                    target="_blank"
                  >
                    <Box
                      component="img"
                      width={100}
                      height={"auto"}
                      alt="Foto"
                      src={item.button}
                      className={classes.imgC}
                    />
                  </Button>
                </Box>

                <Box component={"span"} sx={{ display: "table-row-group" }}>
                  {t("Presentación.Proceso")}
                </Box>
                <Box component={"span"} sx={{ display: "table-row-group" }}>
                  {t("Presentación.Desarrollo")}
                </Box>
              </Box>
            </Box>
          ))}
        </Slider>
      </Container>
    </>
  );
}
