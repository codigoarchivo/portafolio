import { useTranslation } from "react-i18next";

import project1 from "../img/project-1.png";
import project2 from "../img/project-2.png";
import project3 from "../img/project-3.png";
import project4 from "../img/project-4.png";
import project5 from "../img/project-5.png";

import Netlify from "../img/Netlify_logo.svg";
import Heroku from "../img/Heroku_logo.svg";
import Firebase from "../img/firebase_Logo.svg";


export default function CardBase() {
  const [t] = useTranslation("global");

  const content = [
    {
      image: project1,
      title: t("Presentación.titulo1"),
      description: t("Presentación.Proyecto1"),
      button: Netlify,
      hrefPor: "https://boring-borg-5716fa.netlify.app/",
    },
    {
      image: project2,
      title: t("Presentación.titulo2"),
      description: t("Presentación.Proyecto2"),
      button: Netlify,
      hrefPor: "https://flamboyant-cray-41588a.netlify.app/",
    },
    {
      image: project3,
      title: t("Presentación.titulo3"),
      description: t("Presentación.Proyecto3"),
      button: Netlify,
      hrefPor: "https://practical-wescoff-c6cf71.netlify.app/",
    },
    {
      image: project4,
      title: t("Presentación.titulo4"),
      description: t("Presentación.Proyecto4"),
      button: Heroku,
      hrefPor: "https://technology-phone.herokuapp.com/",
    },
    {
      image: project5,
      title: t("Presentación.titulo5"),
      description: t("Presentación.Proyecto5"),
      button: Firebase,
      hrefPor: "https://react-app--project.web.app/",
    },
  ];

  return { content };
}
