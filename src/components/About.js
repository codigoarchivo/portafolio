import React from "react";

import { useTranslation } from "react-i18next";
import { makeStyles } from "@material-ui/core/styles";

import { Container } from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";

import { DivideAbout } from "./Divide";

import fondo from "../img/fondo.jpg";

const useStyles = makeStyles((theme) => ({
  GridCont: {
    [theme.breakpoints.down("sm")]: {
      flexDirection: "column-reverse",
    },
  },
  textCont: {
    padding: "2rem 4rem",
    [theme.breakpoints.down("sm")]: {
      marginBottom: "0rem",
      borderLeft: "none",
      boxShadow: "none",
      padding: ".5rem 1.5rem",
      marginTop: theme.spacing(0),
    },
  },
  text: {
    display: "table",
    textAlign: "center",
    textDecoration: "underline",
    padding: theme.spacing(5, 0),
    margin: "0 auto",
    [theme.breakpoints.down("sm")]: {
      padding: theme.spacing(3, 0),
    },
  },
  paper: {
    display: "flex",
    justifyContent: "center",
    flexDirection: "column",
    alignItems: "center",
    padding: theme.spacing(20, 0),
    [theme.breakpoints.down("sm")]: {
      padding: theme.spacing(0),
    },
  },
  content: {
    width: "77%",
    display: "inline-grid",
    border: "solid 2px #f15025",
    backgroundColor: "#2f2d2e",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(5, 2),
    borderRadius: "5px",
    flexDirection: "column",
    justifyContent: "center",
    [theme.breakpoints.down("md")]: {
      width: "95%",
    },
    [theme.breakpoints.down("sm")]: {
      padding: "1.5rem",
      width: "70%",
    },
    [theme.breakpoints.down("xs")]: {
      width: "95%",
    },
  },
  imag: {
    boxShadow: theme.shadows[5],
    borderRadius: theme.spacing(1),
    backgroundImage: `linear-gradient(
      to right bottom,
      rgba(0, 0, 0, 0.7),
      rgba(0, 0, 0, 0.7)
    ), url(${fondo})`,
    backgroundSize: "cover",
    padding: theme.spacing(8, 5),
    [theme.breakpoints.down("sm")]: {
      width: "100%",
      padding: theme.spacing(4, 2),
    },
  },
  textTypo: {
    [theme.breakpoints.down("sm")]: {
      fontSize: "3rem",
    },
    [theme.breakpoints.down("xs")]: {
      fontSize: "2rem",
      width: "52%",
    },
  },
  sub: {
    [theme.breakpoints.down("xs")]: {
      width: "52%",
    },
  },
  ulCont: {
    display: "contents",
  },
  strongCont: {
    fontSize: "1.2rem",
    margin: theme.spacing(0.5, 0),
    fontWeight: "bold",
  },
  spanCont: {
    margin: theme.spacing(0.5, 0),
    fontSize: "1.2rem",
    marginLeft: ".3rem",
  },
}));
function About() {
  const classes = useStyles();
  const [t] = useTranslation("global");
  return (
    <>
      <Container>
        <DivideAbout />
        <Grid
          container
          className={classes.GridCont}
          justifyContent="center"
          mb={1.5}
          sx={{ position: "relative" }}
        >
          <Grid item sm={12} md={6} className={classes.paper}>
            <Typography
              variant="h4"
              className={classes.text}
              gutterBottom
              color="primary"
            >
              Datos
            </Typography>

            <Box component={"div"} className={classes.content}>
              <Box component={"ul"} className={classes.ulCont}>
                <Box component={"li"} mb={1.5} sx={{ display: "inline-block" }}>
                  <Box component={"strong"} className={classes.strongCont}>
                    EMAIL:
                  </Box>
                  <Box component={"span"} className={classes.spanCont}>
                    codigoarchivo@gmail.com
                  </Box>
                </Box>
                <Box component={"li"} mb={1.5} sx={{ display: "inline-block" }}>
                  <Box component={"strong"} className={classes.strongCont}>
                    {t("Presentación.TELÉFONO")}:
                  </Box>
                  <Box component={"span"} className={classes.spanCont}>
                    +584126804788
                  </Box>
                </Box>
                <Box component={"li"} mb={1.5} sx={{ display: "inline-block" }}>
                  <Box component={"strong"} className={classes.strongCont}>
                    {t("Presentación.DIRECCIÓN")}:
                  </Box>
                  <Box component={"span"} className={classes.spanCont}>
                    Caracas, Venezuela
                  </Box>
                </Box>
                <Box component={"li"} mb={1.5} sx={{ display: "inline-block" }}>
                  <Box component={"strong"} className={classes.strongCont}>
                    {t("Presentación.IDIOMA")}:
                  </Box>
                  <Box component={"span"} className={classes.spanCont}>
                    {t("Presentación.BásicoAvanzado")}
                  </Box>
                </Box>
              </Box>
            </Box>
          </Grid>
          <Grid item sm={12} md={6} className={classes.imag}>
            <Box component={"div"} sx={{ display: "inline-block" }}>
              <Typography
                color="inherit"
                variant="h2"
                data-aos="fade-left"
                data-aos-duration="1500"
                className={classes.textTypo}
              >
                <Box component="span" sx={{ display: "block" }}>
                  {t("Presentación.Básico")}
                </Box>{" "}
                <Box component="span" sx={{ display: "block" }}>
                  {t("Presentación.Información1")}
                </Box>
              </Typography>
              <Typography
                variant="body2"
                color="inherit"
                data-aos="fade-left"
                data-aos-duration="1500"
                className={classes.sub}
              >
                <Box
                  sx={{
                    display: "block",
                  }}
                  component="small"
                >
                  {t("Presentación.único")}
                </Box>{" "}
                <Box
                  component="small"
                  sx={{ display: "block", fontStyle: "italic" }}
                >
                  Steve Jobs --
                </Box>
              </Typography>
            </Box>
          </Grid>
        </Grid>
      </Container>
    </>
  );
}
export default About;
