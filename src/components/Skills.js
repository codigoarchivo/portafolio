import React from "react";
import { CarouselAndLinearProgressWithLabel } from "./CarouselAndLinearProgressWithLabel";

import { makeStyles } from "@material-ui/core/styles";

import Grid from "@material-ui/core/Grid";

import { Box, Container, Typography } from "@material-ui/core";

import { DivideSkill } from "./Divide";

import fondo from "../img/chess.jpg";
import { useTranslation } from "react-i18next";

const useStyles = makeStyles((theme) => ({
  imag: {
    backgroundImage: `linear-gradient(
      to right bottom,
      rgba(0, 0, 0, 0.6),
      rgba(0, 0, 0, 0.6)
    ), url(${fondo})`,
    boxShadow: theme.shadows[5],
    borderRadius: theme.spacing(1),
    backgroundSize: "cover",
    padding: theme.spacing(8, 5),
    [theme.breakpoints.down("sm")]: {
      width: "100%",
      padding: theme.spacing(4, 2),
    },
  },
  paper: {
    padding: theme.spacing(10, 0),
    [theme.breakpoints.down("sm")]: {
      padding: theme.spacing(1, 0),
    },
  },
  textTypo: {
    [theme.breakpoints.down("sm")]: {
      fontSize: "3rem",
    },
    [theme.breakpoints.down("xs")]: {
      fontSize: "2rem",
      width: "52%",
    },
  },
  sub: {
    [theme.breakpoints.down("xs")]: {
      width: "60%",
    },
  },
}));
export const Skills = () => {
  const classes = useStyles();
  const [t] = useTranslation("global");
  return (
    <>
      <Container>
        {/* components > Divide */}
        <DivideSkill />
        <Grid
          container
          justifyContent="center"
          style={{ position: "relative" }}
        >
          <Grid item sm={12} md={6} className={classes.imag}>
            <Box component={"div"} sx={{ display: "inline-block" }}>
              <Typography
                variant="h2"
                color="inherit"
                data-aos-duration="1500"
                data-aos="fade-right"
                className={classes.textTypo}
              >
                <Box component="span" sx={{ display: "block" }}>
                  {t("Presentación.Mis")}
                </Box>{" "}
                <Box component="span" sx={{ display: "block" }}>
                  {t("Presentación.Habilidad")}
                </Box>
              </Typography>
              <Typography
                variant="body2"
                color="inherit"
                data-aos-duration="1500"
                data-aos="fade-right"
                className={classes.sub}
              >
                <Box component="small" sx={{ display: "block" }}>
                  {t("Presentación.esfuerzo")}
                </Box>
                <Box
                  component="small"
                  sx={{ display: "block", fontStyle: "italic" }}
                >
                  James Whitcomb Riley --
                </Box>
              </Typography>
            </Box>
          </Grid>
          <Grid item sm={12} md={6} className={classes.paper}>
            {/* components > CarouselAndLinearProgressWithLabel */}
            <CarouselAndLinearProgressWithLabel />
          </Grid>
        </Grid>
      </Container>
    </>
  );
};
