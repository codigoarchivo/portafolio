import React from "react";

import Typed from "typed.js";

import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";

import { makeStyles } from "@material-ui/core/styles";

import { useTranslation } from "react-i18next";

import buildings from "../img/buildings.jpg";

import { CardCont } from "../components/CardCont";

import "../index";
import { ParticleContent } from "../animations/ParticleContent";

const useStyles = makeStyles((theme) => ({
  header: {
    height: "auto",
    backgroundImage: `linear-gradient(
      to right bottom,
      rgba(0, 0, 0, 0.7),
      rgba(0, 0, 0, 0.7)
    ),url(${buildings})`,
    boxShadow: theme.shadows[5],
    backgroundSize: "cover",
    backgroundPosition: "center center",
    flexWrap: "wrap",
    alignContent: "space-evenly",
    position: "relative",
  },
  box_cont: {
    padding: theme.spacing(3, 0),
    height: "100vh",
    minHeight: "100vh",
    [theme.breakpoints.down("sm")]: {
      height: "auto",
      minHeight: "auto",
    },
  },
  Gridheader: {},
  textCont: {
    padding: theme.spacing(2),
    [theme.breakpoints.down("sm")]: {
      padding: theme.spacing(2, 1, 2, 1),
    },
    // [theme.breakpoints.down("xs")]: {
    //   marginLeft: theme.spacing(1),
    //   marginTop: theme.spacing(2),
    // },
  },
  /* Text */
  titleHeader: {
    display: "initial",
    color: "#f15025",
    fontSize: "5rem",
    marginLeft: theme.spacing(5),
    fontWeight: "bold",
    [theme.breakpoints.down("md")]: {
      fontSize: "3rem",
    },
    [theme.breakpoints.down("sm")]: {
      marginLeft: theme.spacing(6),
      fontSize: "2.5rem",
    },
    [theme.breakpoints.down("xs")]: {
      fontSize: "2rem",
    },
  },
  segund: {
    height: "5px",
    width: "10%",
    display: "inline-block",
    backgroundColor: "#fff",
    marginLeft: theme.spacing(6),
  },
  headerCont: {
    display: "block",
    wordWrap: "break-word",
    visibility: "visible",
    fontSize: "1.2rem",
    textIndent: "2rem",
    lineHeight: "2",
    wordBreak: "break-word",
    marginLeft: theme.spacing(2),
    [theme.breakpoints.down("sm")]: {
      marginLeft: theme.spacing(0),
      fontSize: "1rem",
    },
    [theme.breakpoints.down("xs")]: {
      textIndent: "1.6rem",
    },
  },
}));

export default function BackToTop() {
  const classes = useStyles();

  const [t] = useTranslation("global");

  // Create reference to store the DOM element containing the animation
  const el = React.useRef(null);
  // Create reference to store the Typed instance itself
  const typed = React.useRef(null);

  React.useEffect(() => {
    const options = {
      strings: ["Objetivo", "Profesional", "Objetive", "Professional"],
      typeSpeed: 60,
      backSpeed: 60,
      smartBackspace: true,
      backDelay: 1200,
      loop: true,
      loopCount: Infinity,
    };

    // elRef refers to the <span> rendered below
    typed.current = new Typed(el.current, options);

    return () => {
      // Make sure to destroy Typed instance during cleanup
      // to prevent memory leaks
      typed.current.destroy();
    };
  }, []);

  return (
    <>
      <Box
        id="back-to-top-anchor"
        className={classes.header}
        component={"header"}
      >
        <Box component={"div"} className={classes.box_cont}>
          <ParticleContent />
          <Grid
            container
            className={classes.Gridheader}
            justifyContent="center"
            alignContent="center"
            alignItems="center"
            style={{ height: "-webkit-fill-available" }}
          >
            <Grid item xs={12} sm={5} md={5} lg={5} xl={5}>
              {/* components > CardCont */}
              <CardCont />
            </Grid>
            <Grid item xs={12} sm={7} md={7} lg={6} xl={6}>
              <Box component={"div"} className={classes.textCont}>
                <Box
                  component={"div"}
                  className={("type-wrap", classes.titleHeader)}
                >
                  <Box component={"span"} sx={{ whiteSpace: "pre" }} ref={el} />
                </Box>

                <Box component={"div"}>
                  <Box component={"div"} className={classes.segund}></Box>
                </Box>

                <Typography className={classes.headerCont}>
                  {<b>{t("Presentación.nombre")} </b>}
                  {t("Presentación.Presentación")}
                </Typography>
              </Box>
            </Grid>
          </Grid>
        </Box>
      </Box>
    </>
  );
}
