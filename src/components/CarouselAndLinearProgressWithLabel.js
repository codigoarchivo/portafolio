import React from "react";
import SwipeableViews from "react-swipeable-views";

import { autoPlay } from "react-swipeable-views-utils";

import { makeStyles } from "@material-ui/core/styles";
import { KeyboardArrowLeft, KeyboardArrowRight } from "@material-ui/icons";
import {
  Box,
  Button,
  MobileStepper,
  Typography,
  useTheme,
} from "@material-ui/core";

import { Progress } from "../helpers/Progress";
import { NextAndPrev } from "../helpers/NextAndPrev";
import { Portafolio } from "../helpers/Portafolio";

const useStyles = makeStyles((theme) => ({
  root: {
    border: "solid 2px #f15025",
    borderRadius: "5px",
    backgroundColor: "#2f2d2e",
    boxShadow: theme.shadows[5],
  },
  SpaceCard: {
    maxWidth: 400,
    margin: "0 auto",
    [theme.breakpoints.down("xs")]: {
      maxWidth: 260,
    },
  },
  textCont: {
    fontSize: "1.6rem",
    textDecoration: "underline",
    fontWeight: 600,
    padding: theme.spacing(5, 0),
    display: "table",
    margin: "0 auto",
  },
  item: {
    padding: theme.spacing(2),
  },
  box: {
    backgroundColor: "transparent",
  },
}));

const AutoPlaySwipeableViews = autoPlay(SwipeableViews);

export function CarouselAndLinearProgressWithLabel() {
  const classes = useStyles();
  const theme = useTheme();

  const { portafolio } = Portafolio();

  const maxSteps = portafolio.length;
  // helper > NextAndPrev
  const {
    activeStep,
    handleNext,
    handleBack,
    handleStepChange,
  } = NextAndPrev();
  return (
    <>
      <Box component={"div"} className={classes.SpaceCard}>
        <Typography className={classes.textCont} color="primary">
          {portafolio[activeStep].title}
        </Typography>
        <AutoPlaySwipeableViews
          axis={theme.direction === "rtl" ? "x-reverse" : "x"}
          index={activeStep}
          onChangeIndex={handleStepChange}
          interval={5000}
          enableMouseEvents
        >
          {portafolio.map((step, index) => (
            <Box component={"div"} className={classes.root} key={step.label1}>
              {Math.abs(activeStep - index) <= 8 ? (
                <Box component={"div"} className={classes.item}>
                  <Progress
                    lenguaje={step.label1}
                    porcentaje={step.porcent1}
                    value={100}
                  />
                  <Progress
                    lenguaje={step.label2}
                    porcentaje={step.porcent2}
                    value={step.label2 ? 100 : ""}
                  />
                  <Progress
                    lenguaje={step.label3}
                    porcentaje={step.porcent3}
                    value={step.label3 ? 100 : ""}
                  />
                  <Progress
                    lenguaje={step.label4}
                    porcentaje={step.porcent4}
                    value={step.label4 ? 100 : ""}
                  />
                  <Progress
                    lenguaje={step.label5}
                    porcentaje={step.porcent5}
                    value={step.label5 ? 100 : ""}
                  />
                  <Progress
                    lenguaje={step.label6}
                    porcentaje={step.porcent6}
                    value={step.label6 ? 100 : ""}
                  />
                  <Progress
                    lenguaje={step.label7}
                    porcentaje={step.porcent7}
                    value={step.label7 ? 100 : ""}
                  />
                  <Progress
                    lenguaje={step.label8}
                    porcentaje={step.porcent8}
                    value={step.label8 ? 100 : ""}
                  />
                </Box>
              ) : null}
            </Box>
          ))}
        </AutoPlaySwipeableViews>

        <MobileStepper
          className={classes.box}
          steps={maxSteps}
          position="static"
          activeStep={activeStep}
          nextButton={
            <Button
              className={classes.button}
              size="small"
              onClick={handleNext}
              disabled={activeStep === maxSteps - 1}
              color="primary"
            >
              Next
              {theme.direction === "rtl" ? (
                <KeyboardArrowLeft />
              ) : (
                <KeyboardArrowRight />
              )}
            </Button>
          }
          backButton={
            <Button
              className={classes.button}
              size="small"
              onClick={handleBack}
              disabled={activeStep === 0}
              color="primary"
            >
              {theme.direction === "rtl" ? (
                <KeyboardArrowRight />
              ) : (
                <KeyboardArrowLeft />
              )}
              Back
            </Button>
          }
        />
      </Box>
    </>
  );
}
