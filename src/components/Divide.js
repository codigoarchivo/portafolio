import React from "react";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import { useTranslation } from "react-i18next";
import { Box } from "@material-ui/core";
export const DivideAbout = () => {
  const useStyles = makeStyles((theme) => ({
    spaceTB: {
      display: "table",
      margin: "0 auto",
      padding: theme.spacing(8, 0),
      [theme.breakpoints.down("sm")]: {
        padding: theme.spacing(3, 0),
      },
    },
    titulo: {
      fontWeight: 900,
      fontFamily: "Haas Grot Text R Web Helvetica Neue sans-serif",
      textAlign: "center",
      textShadow:
        "2px 7px 5px rgba(0,0,0,0.3), 0px -4px 10px rgba(255,255,255,0.3)",
      [theme.breakpoints.down("sm")]: {
        fontSize: "3rem",
      },
      [theme.breakpoints.down("xs")]: {
        fontSize: "2.2rem",
      },
    },
    subtitulo: {
      textAlign: "center",
      [theme.breakpoints.down("sm")]: {
        fontSize: "1rem",
      },
    },
  }));

  const classes = useStyles();
  const [t] = useTranslation("global");
  return (
    <>
      <Box
        component={"div"}
        className={classes.spaceTB}
        sx={{ position: "relative" }}
      >
        <Box
          sx={{ top: "-4rem", position: "absolute" }}
          component={"div"}
          id="information"
        ></Box>
        <Typography
          data-aos="zoom-out-up"
          data-aos-duration="1000"
          variant="h2"
          color="primary"
          className={classes.titulo}
        >
          {t("Presentación.Contacto")}
        </Typography>
        <Typography
          data-aos="zoom-out-up"
          data-aos-duration="1000"
          variant="h5"
          color="primary"
          className={classes.subtitulo}
        >
          {t("Presentación.Información")}
        </Typography>
      </Box>
    </>
  );
};
export const DivideSkill = () => {
  const useStyles = makeStyles((theme) => ({
    spaceTB: {
      display: "table",
      margin: "0 auto",
      padding: theme.spacing(8, 0),
      [theme.breakpoints.down("sm")]: {
        padding: theme.spacing(3, 0),
      },
    },
    titulo: {
      fontWeight: 900,
      fontFamily: "Haas Grot Text R Web Helvetica Neue sans-serif",
      textAlign: "center",
      textShadow:
        "2px 7px 5px rgba(0,0,0,0.3), 0px -4px 10px rgba(255,255,255,0.3)",
      [theme.breakpoints.down("sm")]: {
        fontSize: "3rem",
      },
      [theme.breakpoints.down("xs")]: {
        fontSize: "2.2rem",
      },
    },
    subtitulo: {
      textAlign: "center",
      [theme.breakpoints.down("sm")]: {
        fontSize: "1rem",
      },
    },
  }));

  const classes = useStyles();
  const [t] = useTranslation("global");
  return (
    <>
      <Box
        component={"div"}
        className={classes.spaceTB}
        sx={{ position: "relative" }}
      >
        <Box
          sx={{ top: "-4rem", position: "absolute" }}
          component={"div"}
          id="Professional"
        ></Box>
        <Typography
          data-aos="zoom-out-up"
          data-aos-duration="1000"
          variant="h2"
          color="primary"
          className={classes.titulo}
        >
          {t("Presentación.Capacidades")}
        </Typography>
        <Typography
          data-aos="zoom-out-up"
          data-aos-duration="1000"
          variant="h5"
          color="primary"
          className={classes.subtitulo}
        >
          {t("Presentación.Profesional1")}
        </Typography>
      </Box>
    </>
  );
};
export const DividePortfolio = () => {
  const useStyles = makeStyles((theme) => ({
    spaceTB: {
      display: "table",
      margin: "0 auto",
      padding: theme.spacing(8, 0),
      [theme.breakpoints.down("sm")]: {
        padding: theme.spacing(3, 0),
      },
    },
    titulo: {
      fontWeight: 900,
      fontFamily: "Haas Grot Text R Web Helvetica Neue sans-serif",
      textAlign: "center",
      textShadow:
        "2px 7px 5px rgba(0,0,0,0.3), 0px -4px 10px rgba(255,255,255,0.3)",
      [theme.breakpoints.down("sm")]: {
        fontSize: "3rem",
      },
      [theme.breakpoints.down("xs")]: {
        fontSize: "2.2rem",
      },
    },
    subtitulo: {
      textAlign: "center",
      [theme.breakpoints.down("sm")]: {
        fontSize: "1rem",
      },
    },
  }));

  const classes = useStyles();
  const [t] = useTranslation("global");
  return (
    <>
      <Box
        component={"div"}
        className={classes.spaceTB}
        sx={{ position: "relative" }}
      >
        <Box
          sx={{ top: "-4rem", position: "absolute" }}
          component={"div"}
          id="Portfolios"
        ></Box>
        <Typography
          data-aos="zoom-out-up"
          data-aos-duration="1000"
          variant="h2"
          color="primary"
          className={classes.titulo}
        >
          {t("Presentación.Portafolio")}
        </Typography>
        <Typography
          data-aos="zoom-out-up"
          data-aos-duration="1000"
          variant="h5"
          color="primary"
          className={classes.subtitulo}
        >
          {t("Presentación.Proyectos")}
        </Typography>
      </Box>
    </>
  );
};
export const DivideExperience = () => {
  const useStyles = makeStyles((theme) => ({
    spaceTB: {
      display: "table",
      margin: "0 auto",
      padding: theme.spacing(8, 0),
      [theme.breakpoints.down("sm")]: {
        padding: theme.spacing(3, 0),
      },
    },
    titulo: {
      fontWeight: 900,
      fontFamily: "Haas Grot Text R Web Helvetica Neue sans-serif",
      textAlign: "center",
      textShadow:
        "2px 7px 5px rgba(0,0,0,0.3), 0px -4px 10px rgba(255,255,255,0.3)",
      [theme.breakpoints.down("sm")]: {
        fontSize: "3rem",
      },
      [theme.breakpoints.down("xs")]: {
        fontSize: "2.2rem",
      },
    },
    subtitulo: {
      textAlign: "center",
      [theme.breakpoints.down("sm")]: {
        fontSize: "1rem",
      },
    },
  }));

  const classes = useStyles();
  const [t] = useTranslation("global");
  return (
    <>
      <Box
        component={"div"}
        className={classes.spaceTB}
        sx={{ position: "relative" }}
      >
        <Box
          sx={{ top: "-4rem", position: "absolute" }}
          component={"div"}
          id="Experience"
        ></Box>
        <Typography
          data-aos="zoom-out-up"
          data-aos-duration="1000"
          variant="h2"
          color="primary"
          className={classes.titulo}
        >
          {t("Presentación.Experience")}
        </Typography>
        <Typography
          data-aos="zoom-out-up"
          data-aos-duration="1000"
          variant="h5"
          color="primary"
          className={classes.subtitulo}
        >
          {t("Presentación.Profesional2")}
        </Typography>
      </Box>
    </>
  );
};
export const DivideEducation = () => {
  const useStyles = makeStyles((theme) => ({
    spaceTB: {
      display: "table",
      margin: "0 auto",
      padding: theme.spacing(8, 0),
      [theme.breakpoints.down("sm")]: {
        padding: theme.spacing(3, 0),
      },
    },
    titulo: {
      fontWeight: 900,
      fontFamily: "Haas Grot Text R Web Helvetica Neue sans-serif",
      textAlign: "center",
      textShadow:
        "2px 7px 5px rgba(0,0,0,0.3), 0px -4px 10px rgba(255,255,255,0.3)",
      [theme.breakpoints.down("sm")]: {
        fontSize: "3rem",
      },
      [theme.breakpoints.down("xs")]: {
        fontSize: "2.2rem",
      },
    },
    subtitulo: {
      textAlign: "center",
      [theme.breakpoints.down("sm")]: {
        fontSize: "1rem",
      },
    },
  }));

  const classes = useStyles();
  const [t] = useTranslation("global");
  return (
    <>
      <Box component={"div"} className={classes.spaceTB}>
        <Typography
          data-aos="zoom-out-up"
          data-aos-duration="1000"
          variant="h2"
          color="primary"
          className={classes.titulo}
        >
          {t("Presentación.estudio")}
        </Typography>
        <Typography
          data-aos="zoom-out-up"
          data-aos-duration="1000"
          id={"misEstudios"}
          variant="h5"
          color="primary"
          className={classes.subtitulo}
        >
          {t("Presentación.profe")}
        </Typography>
      </Box>
    </>
  );
};
