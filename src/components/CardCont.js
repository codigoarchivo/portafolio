import React from "react";

import { useTranslation } from "react-i18next";

import perfil from "../img/perfil.png";

import Pdf1 from "../img/curriculun_Español.pdf";
import Pdf2 from "../img/curriculun_English.pdf";

import LinkedIn from "../img/LinkedIn_Logo.svg";

import {
  Box,
  Button,
  Card,
  CardActions,
  CardContent,
  CardMedia,
  makeStyles,
  Typography,
} from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  max: {
    backgroundColor: "transparent",
    boxShadow: "none",
    maxWidth: 450,
    [theme.breakpoints.down("md")]: {
      maxWidth: 400,
      margin: "0 auto",
    },
    [theme.breakpoints.down("sm")]: {
      maxWidth: 280,
    },
    [theme.breakpoints.down("xs")]: {
      maxWidth: 200,
    },
  },
  media: {
    height: 0,
    boxShadow: theme.shadows[10],
    paddingTop: "100%",
    [theme.breakpoints.down("sm")]: {
      paddingTop: "90%",
    },
  },
  fondBorde: {
    backgroundColor: "#2f2d2e",
    boxShadow: theme.shadows[1],
    border: "solid 2px #f15025",
    borderRadius: "5px",
    color: "#fff",
  },
  cardContent: {
    [theme.breakpoints.down("xs")]: {
      padding: theme.spacing(2),
    },
  },
  fondoC: {
    justifyContent: "space-evenly;",
    padding: theme.spacing(2),
    [theme.breakpoints.down("sm")]: {
      padding: theme.spacing(1),
    },
  },
  fondoT: {
    display: "initial",
    [theme.breakpoints.down("sm")]: {
      fontSize: "1.2rem",
    },
  },
  Linked: {
    [theme.breakpoints.down("sm")]: {
      height: "18px",
    },
    [theme.breakpoints.down("xs")]: {
      height: "16px",
    },
  },
}));
export const CardCont = () => {
  const classes = useStyles();
  const [t, i18n] = useTranslation("global");
  return (
    <>
      <Card className={classes.max}>
        <CardMedia
          className={classes.media}
          image={perfil}
          title="Contemplative Reptile"
        />
        <Box component={"div"} className={classes.fondBorde}>
          <CardContent className={classes.cardContent}>
            <Typography className={classes.fondoT} variant="h5" component="h2">
              ING. Jackson Quintero
            </Typography>
          </CardContent>

          <CardActions className={classes.fondoC}>
            <Button
              href={i18n.language === "es" ? Pdf1 : Pdf2}
              target="_blank"
              variant="outlined"
              size="small"
              color="primary"
            >
              <Box
                sx={{
                  color: "#fff",
                }}
                component={"span"}
              >
                {t("Presentación.Descargar")}
              </Box>
            </Button>
            <Button
              href="https://www.linkedin.com/in/jackson-quintero/"
              target="_blank"
              size="small"
            >
              <Box
                component={"img"}
                sx={{ height: "23px" }}
                src={LinkedIn}
                alt="Foto"
                className={classes.Linked}
              ></Box>
            </Button>
          </CardActions>
        </Box>
      </Card>
    </>
  );
};
