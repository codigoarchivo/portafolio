import { createTheme } from "@material-ui/core/styles";
export default createTheme({
  palette: {
    primary: {
      main: "#f15025",
    },
    secondary: {
      main: "#2f2d2e",
    },
  },
});
