import React from "react";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import CircularProgress from "@material-ui/core/CircularProgress";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    position: "relative",
    height: 47,
    width: "100%",
  },
  textCon: {
    fontWeight: 900,
    fontSize: "1rem",
  },
  posit: {
    position: "relative",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  progre: {
    position: "absolute",
  },
}));
// components > CarouselAndLinearProgressWithLabel
export const Progress = ({ lenguaje, porcentaje, value }) => {
  const classes = useStyles();
  return (
    <>
      {value ? (
        <Box component={"div"} className={classes.root}>
          <Typography className={classes.textCon}>{lenguaje}</Typography>

          <Box component={"div"} className={classes.posit}>
            <Typography variant="body2" className={classes.porcen}>
              {`${Math.round(value >= porcentaje ? porcentaje : value)}%`}
            </Typography>
            <CircularProgress
              color="primary"
              className={classes.progre}
              variant="determinate"
              value={value >= porcentaje ? porcentaje : value}
            />
          </Box>
        </Box>
      ) : null}
    </>
  );
};
