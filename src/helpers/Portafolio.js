import { useTranslation } from "react-i18next";

export const Portafolio = () => {
  const [t] = useTranslation("global");

  const portafolio = [
    {
      title: t("Presentación.desarrollo"),
      label1: "HTML",
      porcent1: 90,
      label2: "CSS",
      porcent2: 90,
      label3: "SASS",
      porcent3: 80,
      label4: "PHOTOSHOP",
      porcent4: 65,
    },
    {
      title: t("Presentación.Frameword"),
      label1: "VUE.JS",
      porcent1: 70,
      label2: "REACT.JS",
      porcent2: 80,
      label3: "EXPRESS",
      porcent3: 80,
      label4: "REDUX",
      porcent4: 80,
      label5: "VUETIFY",
      porcent5: 70,
      label6: "MATERIAL UI",
      porcent6: 70,
      label7: "ANT DESING",
      porcent7: 60,
      label8: "BOOTSTRAP",
      porcent8: 80,
    },
    {
      title: t("Presentación.Lenguajes"),
      label1: "JAVASCRIPT",
      porcent1: 80,
      label2: "PHYTON",
      porcent2: 55,
    },
    {
      title: t("Presentación.Control"),
      label1: "GIT",
      porcent1: 75,
      label2: "GITHUB",
      porcent2: 72,
      label3: "BITBUCKET",
      porcent3: 70,
    },
    {
      title: t("Presentación.Entornos"),
      label1: "NODE JS",
      porcent1: 70,
    },
    {
      title: "BD relacional y no relacional",
      label1: "MONGODB",
      porcent1: 70,
      label2: "MYSQL",
      porcent2: 50,
    },
  ];

  return { portafolio };
};
