import { withStyles } from "@material-ui/core/styles";

export const GlobalCss = withStyles({
  "@global": {
    html: {
      "@media (max-width: 960px)": {
        fontSize: "90%",
      },
      "@media (max-width: 600px)": {
        fontSize: "75%",
      },
      "@media (max-width: 320px)": {
        fontSize: "65%",
      },
    },

    body: {
      color: "#fff",
    },
    "::selection": {
      backgroundColor: "#f15025",
    },
    ".cssfx-filling-bottle": {
      width: "50px",
    },
    ".slider": {
      margin: "auto",
      position: "relative",
      width: "100%",
      overflow: "hidden",
      borderRadius: "8px",
      boxShadow:
        "0px 3px 5px -1px rgb(0 0 0 / 20%), 0px 5px 8px 0px rgb(0 0 0 / 14%), 0px 1px 14px 0px rgb(0 0 0 / 12%)",
    },
    ".slide h2": {
      transition: "all 0.3s ease",
      "-webkit-transform": "translateY(-20px)",
      transform: "translateY(-20px)",
      opacity: 0,
    },

    ".slide a": {
      transition: "all 0.3s ease",
      "-webkit-transform": "translateY(20px)",
      transform: "translateY(20px)",
      opacity: 0,
    },

    ".slide p": {
      transition: "all 0.3s ease",
      "-webkit-transform": "translateY(20px)",
      transform: "translateY(20px)",
      opacity: 0,
    },

    ".slide span": {
      transition: "all 0.3s ease",
      "-webkit-transform": "translateY(20px)",
      transform: "translateY(20px)",
      opacity: 0,
    },

    ".slide.animateIn.previous h2, .slide.current h2, .slide.animateIn.next h2, .slide.animateIn.previous a,.slide.current a, .slide.animateIn.next a,.slide.animateIn.previous p, .slide.current p, .slide.animateIn.next p, .slide.animateIn.previous span, .slide.current span, .slide.animateIn.next span": {
      "-webkit-transform": "translateX(0)",
      transform: "translateX(0)",
      "-webkit-transition-delay": "0.9s",
      transitionDelay: "0.9s",
      opacity: 1,
    },

    ".slide.animateOut h2": {
      "-webkit-transition-delay": "0.3s",
      transitionDelay: "0.3s",
    },

    ".slide.animateIn.previous p, .slide.animateIn.next p": {
      "-webkit-transition-delay": "1.1s",
      transitionDelay: "1.1s",
    },

    ".slide.animateIn.previous a, .slide.animateIn.next a": {
      "-webkit-transition-delay": "1.3s",
      transitionDelay: "1.3s",
    },

    ".slide.animateIn.previous span, .slide.animateIn.next span": {
      "-webkit-transition-delay": "1.4s",
      transitionDelay: "1.4s",
    },

    polygon: {
      fill: "#f15025",
    },
  },
})(() => null);
